#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <algorithm>
using namespace std;

// config
#define SIZE 8
#define TERRITORY 3
// players
#define IA 0
#define HUMAN 1
#define opponent(x) 1-x 
// exceptions
#define NOT_YOUR_PIECE 1
#define OUT_OF_BOARD 2
#define SQUARE_OCUPIED 3
#define WRONG_MOVE 4

typedef vector<char> vc;
typedef pair<int, int> ii;

class DraughtsBoard{
    friend class DraughtsIA;
public:
    DraughtsBoard(){
        memset(player, 0, sizeof player);
        for(int i=0; i<TERRITORY; ++i) for(int j=0; j<SIZE; ++j)
            if( (i+j)&1 ) put_piece(IA, i, j);
        for(int i=SIZE-TERRITORY; i<SIZE; ++i) for(int j=0; j<SIZE; ++j)
            if( (i+j)&1 ) put_piece(HUMAN, i, j);
        draw();
    }
    void ia_move(int player_id, ii from, ii to){
        remove_piece(player_id, from.first, from.second);
        put_piece(player_id, to.first, to.second);
    }
    void human_move(ii from, ii to){
        if( !square_exists(from.first, from.second) || !square_exists(to.first, to.second) ) throw OUT_OF_BOARD;
        if( !is_piece(HUMAN, from.first, from.second) ) throw NOT_YOUR_PIECE;
        if( is_piece(IA, to.first, to.second) || is_piece(HUMAN, to.first, to.second) ) throw SQUARE_OCUPIED;
        if( to.first == from.first - 1 && abs(to.second - from.second)==1 ){
            remove_piece(HUMAN, from.first, from.second);
            put_piece(HUMAN, to.first, to.second);
        }
        else if( to.first == from.first - 2 && abs(to.second - from.second)==2 ){
            if( !is_piece(opponent(HUMAN), from.first-1, (from.second+to.second)/2) ) throw WRONG_MOVE;
            remove_piece(HUMAN, from.first, from.second);
            put_piece(HUMAN, to.first, to.second);
            remove_piece( opponent(HUMAN), from.first-1, (from.second+to.second)/2);
        }
        else throw WRONG_MOVE;
    }
    bool square_exists(int i, int j) const{
        return 0<=i && i<SIZE && 0<=j && j<SIZE;
    }
    bool is_empty(int i, int j) const{
        return !is_piece(HUMAN, i, j) && !is_piece(IA, i, j);
    }
    bool is_piece(int player_id, int i, int j) const{
        return player[player_id][i] & (1<<j);
    }
    void remove_piece(int player_id, int i, int j){
        player[player_id][i] &= ~(1<<j);
    }
    void put_piece(int player_id, int i, int j){
        player[player_id][i] |= (1<<j);
    }
    int count_human_pieces() const{
        int ret = 0;
        for(int i=0; i<SIZE; ++i) for(int j=0; j<SIZE; ++j)
            ret += is_piece(HUMAN, i, j);
        return ret;
    }
    int count_ia_pieces() const{
        int ret = 0;
        for(int i=0; i<SIZE; ++i) for(int j=0; j<SIZE; ++j)
            ret += is_piece(IA, i, j);
        return ret;
    }
    void draw() const{
        printf(" "); for(int i=0; i<SIZE; ++i) printf(" %d", i); printf("\n");
        for(int i=0; i<SIZE; ++i){
            printf("%d", i);
            for(int j=0; j<SIZE; ++j){
                if(is_piece(IA, i, j)) printf("|x");
                else if(is_piece(HUMAN, i, j)) printf("|o");
                else printf("| ");
            }
            printf("|\n");
        }
        printf("human: %d\t\tia: %d\n", count_human_pieces(), count_ia_pieces());
    }
private:
    short player[2][SIZE];
};



class DraughtsIA{
public:
    DraughtsIA(int _level): level(_level){}

    DraughtsBoard play(const DraughtsBoard& board){
        vector<DraughtsBoard> moves = generate_moves(board);
        int better = assess(1, moves[0]);
        DraughtsBoard* ret = &moves[0];
        for(int i=1; i<moves.size(); ++i){
            int cur = assess(1, moves[i]);
            if(cur > better){
                better = cur;
                ret = &moves[i];
            }
        }
        printf("my better is: %d\n", better);
        return *ret;
    }
private:
    int level;

    vector<DraughtsBoard> generate_moves(const DraughtsBoard& board){
        vector<DraughtsBoard> moves;
        for(int i=SIZE-2; i>=0; --i) for(int j=0; j<SIZE; ++j) if(board.is_piece(IA, i, j)){
            if( board.square_exists(i+1, j-1) ){
                if(board.is_piece(HUMAN, i+1, j-1)){
                    if( board.square_exists(i+2, j-2) && board.is_empty(i+2, j-2) ){
                        DraughtsBoard tb(board); // tb:temporal_board
                        tb.ia_move(IA, ii(i, j), ii(i+2, j-2));
                        tb.remove_piece(HUMAN, i+1, j-1);
                        moves.push_back(tb);
                    }
                }
                else if(board.is_empty(i+1, j-1)){
                    DraughtsBoard tb(board); // tb:temporal_board
                    tb.ia_move(IA, ii(i,j), ii(i+1, j-1));
                    moves.push_back(tb);
                }
            }
            if( board.square_exists(i+1, j+1) ){
                if(board.is_piece(HUMAN, i+1, j+1)){
                    if( board.square_exists(i+2, j+2) && board.is_empty(i+2, j+2) ){
                        DraughtsBoard tb(board); // tb:temporal_board
                        tb.ia_move(IA, ii(i, j), ii(i+2, j+2));
                        tb.remove_piece(HUMAN, i+1, j+1);
                        moves.push_back(tb);
                    }
                }
                else if(board.is_empty(i+1, j+1)){
                    DraughtsBoard tb(board); // tb:temporal_board
                    tb.ia_move(IA, ii(i,j), ii(i+1, j+1));
                    moves.push_back(tb);
                }
            }
        }
        return moves;
    }

    vector<DraughtsBoard> generate_human_moves(const DraughtsBoard& board){
        vector<DraughtsBoard> moves;
        for(int i=1; i<SIZE; ++i) for(int j=0; j<SIZE; ++j) if(board.is_piece(HUMAN, i, j)){
            if( board.square_exists(i-1, j-1) ){
                if(board.is_piece(IA, i-1, j-1)){
                    if( board.square_exists(i-2, j-2) && board.is_empty(i-2, j-2) ){
                        DraughtsBoard tb(board); // tb:temporal_board
                        tb.ia_move(HUMAN, ii(i, j), ii(i-2, j-2));
                        tb.remove_piece(IA, i-1, j-1);
                        moves.push_back(tb);
                    }
                }
                else if(board.is_empty(i-1, j-1)){
                    DraughtsBoard tb(board); // tb:temporal_board
                    tb.ia_move(HUMAN, ii(i,j), ii(i-1, j-1));
                    moves.push_back(tb);
                }
            }
            if( board.square_exists(i-1, j+1) ){
                if(board.is_piece(IA, i-1, j+1)){
                    if( board.square_exists(i-2, j+2) && board.is_empty(i-2, j+2) ){
                        DraughtsBoard tb(board); // tb:temporal_board
                        tb.ia_move(HUMAN, ii(i, j), ii(i-2, j+2));
                        tb.remove_piece(IA, i-1, j+1);
                        moves.push_back(tb);
                    }
                }
                else if(board.is_empty(i-1, j+1)){
                    DraughtsBoard tb(board); // tb:temporal_board
                    tb.ia_move(HUMAN, ii(i,j), ii(i-1, j+1));
                    moves.push_back(tb);
                }
            }
        }
        return moves;
    }


    int assess(const DraughtsBoard& board){
        return board.count_ia_pieces() - board.count_human_pieces();
    }

    int assess(int cur_level, const DraughtsBoard& board){
        if(cur_level == level) return assess(board);
        int val;
        if(~cur_level&1){
            vector<DraughtsBoard> moves = generate_moves(board);
            if(moves.empty()) return assess(board);
            val=assess(cur_level+1, moves[0]);
            for(int i=1; i<moves.size(); ++i)
                val = max(val, assess(cur_level+1, moves[i]));
        }
        else{
            vector<DraughtsBoard> moves = generate_human_moves(board);
            if(moves.empty()) return assess(board);
            val=assess(cur_level+1, moves[0]);
            for(int i=1; i<moves.size(); ++i)
                val = min(val, assess(cur_level+1, moves[i]));
        }
        return val;
    }
};





int main(){
    DraughtsBoard board;
    DraughtsIA ia(6);
    while(1){
        try{
            int a,b,c,d; scanf("%d %d %d %d", &a, &b, &c, &d);
            board.human_move(ii(a,b), ii(c,d));
            board.draw();
            board = ia.play(board);
            board.draw();
        }
        catch(int e){
           printf("wrong move!\n");
        }
    }    
    return 0;
}